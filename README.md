# Workshop Javascript - Entendiendo lo básico

Con el paso de los años, han surgido nuevas herramientas que aseguran facilitarnos el trabajo a la hora de ejecutar Javascript. Pero antes de abordar estos complejos frameworks o herramientas, es necesario conocer los pilares, entender cómo se ejecuta el código y que elementos participan en ello.

Esto nos permitirá ser mejores desarrolladores, pues tendremos en mente todos estos conceptos antes de codificar cualquier línea de código.

¡Empecemos!

PD: Por favor, te invitamos a probar cada extracto de código, copiándolo en la cónsola del navegador Chrome: Puedes acceder desde cualquier ventana de Chrome, haciendo click derecho y seguidamente Inspeccionar o Inspect --> Allí conseguirás una pestaña de nombre Console, al lado de Elements.

---

## 1.- Conceptos básicos de Javascript

### 1.1.- ¿Qué es Javascript?

- Lenguaje con mayor adopción en desarrollo web.
- Permite trabajar en frontend y backend.
- Es un lenguaje interpretado que a bajo nivel ejecuta sentencias en C.
- Para su ejecución en servidor, se utiliza V8 como engine y libuv para el manejo de concurrencia.
- Nodejs es el estándar que se usa para ejecutar javascript a nivel de servidor. NPM es el manejador de dependencias node y se puede manejar el ciclo de vida del proyecto.

- Muchas de las definiciones de JavaScript se rigen a través del estándar **ECMAScript**, que posee las definiciones formales para client-side rendering normado por la organización [ECMA](https://en.wikipedia.org/wiki/Ecma_International). No solo JavaScript se
basa el lenguaje ECMAScript, existen otros como JScript y ActionScript 3 que también lo hacen. 
Haciendo una analogía, diremos que ECMAScript es el lenguaje y JavaScript, JScript y ActionScript 3
son dialectos de este lenguaje, siendo JavaScript su dialecto más conocido y utilizado. Actualmente el dialecto más usado es ES6 o ECMASCRIPT2015.

- **Las variables en Javascript** son case sensitive, es decir distinguen entre mayúsculas y minúsculas. No pueden empezar con numeros ni contener hypen -.
var hola != var HOLA
```
var 99problems;
var hi-problems;
```
- Tiene la característica de ser un lenguaje débilmente tipado. Es decir, no es necesario almacenar el tipo de dato que se va a almacenar en la variable, Javascript lo infiere a medida que se realiza la asignación. 
    ```
    var thisIsAString = 'Soy un string';
    
    thisIsAString = 1; // Se asigna un número a un string
    
    console.log(thisIsAString); // Se imprime el número en cónsola
    ```

- **Hoisting** también entra en juego, dado que es el mecanismo que usa Javascript para mover al inicio del **scope funcional** (alcance que tiene la función) a todas las declaraciones de variables o funciones involucradas en un extracto de código. Si la variable no es definida dentro del scope de una función, inmediatamente se mueve al inicio del scope global. A continuación, algunos ejemplos:

    - **Hoisting de variables:**
    El resultado de esta ejecución será undefined, dado que la declaración fue movida al inicio del global scope.
    ```
    console.log("varHoisted");
    var varHoisted = "Hola soy una declaracion";
    ```

    - **Hoisting de funciones:**
    En este caso, todo el cuerpo de la función es colocado al inicio del global scope. Es por ello que podemos llamar a una función, incluso antes de su declaración y podremos usarla de la misma forma:

    ```
    console.log(funcHoisted());

    function funcHoisted(){
      console.log("Hola! estoy dentro de una función");
    }

    ---------------------
    //La salida será 
    ƒ funcHoisted(){
      console.log("Hola! estoy dentro de una función");
    }

    ```
    **- Hoisting de function expressions:**
    Estás son tratadas de la misma forma que una variable. Retornarán undefined, si son invocadas antes de ser declaradas:
    ```
    console.log(funcExpHoisted);

    var funcExpHoisted = function(){
      console.log("Hola! estoy dentro de una función expresión");
    }
    ```

    Para evitar errores de código, es preferible siempre declarar e inicializar variables al inicio del script, de forma que se evite usar variables con valor undefined.

- **Objetos en javascript:** Casi todas las estructuras que se manejan en Javascript son objetos: Los arreglos, las funciones, las fechas. Todos los valores en Javascript son objetos, exceptuando a los tipos primitivos, que son aquellos tipos de datos que no tienen atributos ni funciones asociadas. Entre estos últimos, podemos conseguir 5 valores: los string, number, boolean, null y undefined.

Los tipos primitivos son inmutables, mientras que los objetos son mutables y podemos crearlos usando la palabra reservada new.

Hay dos formas de crear objetos:

- **Function constructors:** Crea objetos a través de la definición de sus atributos. Se pueden acceder a través de "." o con bracket notation []. Permite crear un objeto que se puede instanciar en distintos instancias mediante la palabra reservada new:
```
function Person(first, last, age, favColor) {
  this.firstName = first;
  this.lastName = last;
  this.age = age;
  this.favColor = favColor;
}

var katty = new Person("Katty", "Samaniego", "30", "verde");

```
- **Object literals**: Objetos creados a partir de un conjunto de pares key/value separados por ":" encerrados por {}, que pueden contener funciones. Con esta notación, creamos un solo objeto, que puede ser interpretado como un singleton: por más que editemos sus variables, siempre se hará sobre la misma estructura.
```
  var person = {
      firstName: "Katty",
      lastname: "Samaniego",
      age: "30",
      favColor: "verde",
      greet: function(){
          console.log("HI!");
      }
  }
  console.log(person.age);
  var person2 = person;
  person2.age = "25";
  console.log(person.age);
  ```

- Todos los objetos de Javascript heredan del objeto **Prototype**. Permite agregar un atributo o métodos a todos los objetos existentes de un tipo en particular. Se accede de igual forma que cualquier otro objeto, a través del "." o "[]"
Definimos un objeto Persona:
  ```
  function Person(first, last, age, favColor) {
    this.firstName = first;
    this.lastName = last;
    this.age = age;
    this.favColor = favColor;
    this.getFullName = function() {
    return this.firstName + " " + this.lastName;
    };
  }

  ```
  Ahora probamos instanciando dos objetos:
  ```
  var katty = new Person("Katty", "Samaniego", "30", "verde");
  var jorge = new Person("Jorge", "Urbina", "30", "negro");
  ```

  **¿Qué ocurre por detrás?**

  Cada vez que se crea una 
  instancia de este objeto, se crea una instancia de esa funcion junto a todos sus atributos y funciones adicionales, esto produce más gasto de memoria.

  Pero si usamos prototypes, el dunder proto o _proto_ atribute, apuntan al objeto prototype
  que a su vez, apunta al constructor del objeto, por lo que podremos mantener una sola implementación para la función getFullName:

  ```
  function Person(first, last, age, favColor) {
    this.firstName = first;
    this.lastName = last;
    this.age = age;
    this.favColor = favColor;
  }
  Person.prototype.getFullName = function() { 
     

    return this.firstName + " " + this.lastName;
  };
  ```
  Ahora, podemos observar que al crear de nuevo los objetos, ambos comparten la implementación de prototype:
  ```
  var katty = new Person("Katty", "Samaniego", "30", "verde");
  var jorge = new Person("Jorge", "Urbina", "30", "negro");

  Person.prototype === katty.__proto__ //true
  katty.__proto__ === jorge.__proto__ //true 
  ```



- **JSON**: Javascript Object Notation: Es un estandar para estructurar data, inspirado por los object literals. Permite intercambiar información con servicios web construidos en distintos lenguajes de programación.
Se diferencian con los objects literals en que sólo mandan datos nunca funciones y sus valores son encerrados en commilas "".


- Hay dos formas de manejar las referencias a una función: **por valor o por referencia**.

    * Por valor:

    Declaramos un valor primitivo :
    var a --> esto ocupa el espacio de memoria 0x001
    En una función usamos ese valor y se lo asignamos a b:
    b = a 
    Pero ahora b, usa otro espacio de memoria 0x002
    Dos espacios de memoria, que ocupan el mismo valor.

    Ejemplo:
    ```
    function change(b){
        b = 2;
    }

    var a = 1;
    change(a);
    console.log(a);// 1
    ```
    ¿Por qué? Porque por ser a un valor primitivo, solo le pasa a b su valor,
    y b ocupa otro espacio de memoria, por tanto a, queda intacto.


    * Por referencia:

    Cuando declaramos un objeto:
    var a = {
        e: hola,
        4: chao
    }
    ocupa espacio de memoria 0x001.

    Si hacemos:

    var b = a; --> ocupa el mismo espacio de memoria 0x001

    ```
    function changeObj(d){
        d.prop1 = {};
        d.prop2 = function() {};
    }

    var c = {};
    c.prop1 = {};
    changeObj(c);
    console.log(c); // c{prop1:function, prop2: {}}
    ```
    Como c es un objeto, se le pasa la referencia a d y este sobreescribe a c.


- **IIFES**: Inmediately Invoked Function Expressions. Son expresiones de función invocadas inmediatamente. Fueron las primeras alternativas para crear módulos, ya que creaba una instancia cerrada con un scope cerrado.

  Basta con crear alguna funcion anónima:
  ```
  function(){
      var name = 'hola';
      console.log(name);
  }
  ```

  Agregándoles los paréntesis, la volvemos una expresión:
  ```
  (function(){
      var name = 'hola';
      console.log(name);
  })
  ```

  Finalmente, agregándole los "()", le decimos que se ejecute inmediatamente:
  ```
  (function(){
      var name = 'hola';
      console.log(name);
  })()

  console.log(name); // Me va a responder ''
  ```
  Si le pongo la misma variable afuera, no va a alterar nada de lo que esté dentro de la expresión.

- Una característica importante al aprender javascript es el entendimiento del concepto “CLOSURE”.
En sencillo: es la posibilidad de que una función interior pueda acceder a atributos de una función que envuelve a la función interior.
```
function inicia() {
  var nombre = "Mozilla"; // 'nombre' es una variable local creada por la función 'inicia'
  function muestraNombre() { // 'muestraNombre' es una función interna (un closure)
    alert(nombre); // dentro de esta función usamos una variable declarada en la función padre
  }
  muestraNombre();
}
inicia();  
```

Ahora revisemos un ejemplo en el que necesitamos usar tanto una iife como un closure:
Necesitamos una función que incremente su valor, cada vez que se invoque desde distintos lugares:
```
var add = (function () {
  var counter = 0;
  return function () {counter += 1; return counter}
})();// en esta ejecución, resuelve la iife

add(); // aquí empieza a aumentar el contador en 1
add(); // aquí suma 2
add(); // retorna finalmente 3

```
- **Eventos**:  No existen en Javascript, lo que hace es reaccionar a esos eventos y ejecutar código javascript para manejarlos. Javascript solo maneja objetos y algunos de esos objetos, manejan eventos como es el caso de:
      - DOM
      - Window
      - Iframe
Ejemplos de estos eventos son:
Cuando un usuario hace un click en una página
Cuando una página termina de cargarse
Cuando una imagen ha sido cargada
Cuando el mouse se mueve encima de un elemento HTML.
Cuando un formulario es completado y enviado al server.



-------------------------------- 
## 2.- Callbacks

Antes de entender que es un callback, debemos entender que es una función de primer orden en Javascript. Éstas, tienen las siguientes características:

- Pueden almacenarse en una variable, un objeto o una variable:

```
 let fn = function doSomething() {}
 let obj = { doSomething : function(){} 
 arr.push(function doSomething() {})
 ```

- Se puede devolver desde una función:
```
  function a() {
      console.log('A');
  }
  // Imprime a A

  function b() {
      console.log('B');
      return a;
  }
  //Imprime 'B', y devuelve la definición de la función a

  function c() {
      console.log('C');
      return a();
  }
  //Imprime 'C' y ejecuta a(), que a su vez imprime 'A'
  Para ejecutar b()(); --> Sólo así imprime B y luego A
  Para ejecutar c(); -> Sólo basta una llamada, ya que c, ejecuta a su vez a a().

```
- Se puede pasar como argumento a otra función:
```
doAction(function doSomething(){});
```
En este caso, doSomething es un callback. Los callback son funciones pasados como argumento hacia otra función.

Ahora, veamos otro ejemplo de funciones de este tipo:
```
console.log("---------------------------------- callback examples ------------------------------------------");

function contengoCallback(a, b, callback) {

  console.log("El valor de a es:", a);
  console.log("El valor de b es:", b);
  let c = a + b;

  console.debug("Lo siguiente pertenece al callback");
  return callback(c);
}

function callbackCustom(c) {
  console.log("BIENVENIDOS AL CALLBACK");
  console.log("ARGUMENTO DEL CALLBACK: ", c);
  return "FINALIZANDO EL TRABAJO DEL CALLBACK con arg: " + c;
}

let resultadoCallback = contengoCallback(123, 456, callbackCustom);
console.log("Que dijo el callback: ", resultadoCallback);

```

La salida de esta ejecución, nos resultará en: 

Salida de Consola del ejemplo:
```
---------------------------------- callback examples ------------------------------------------
El valor de a es: 123
El valor de b es: 456
Lo siguiente pertenece al callback
BIENVENIDOS AL CALLBACK
ARGUMENTO DEL CALLBACK:  579
Que dijo el callback:  FINALIZANDO EL TRABAJO DEL CALLBACK con arg: 579
```


-------------------------------- 
## 3.- Programación bloqueante y no bloqueante
NOTA: Para ejecutar los ejemplos de esta sección, haremos uso de un [playground de Node](https://repl.it/repls/WretchedCruelDirectories).

Las funciones según el uso de los recursos pueden ser:

 - Bloqueantes 
 
    - También se conocen como síncronas.
    - No libera recursos hasta terminar.

    Cuando una tarea está tomando en ejecutarse más de lo normal, nuestro navegador puede lanzar el siguiente mensaje:
    ![Ejecución Bloqueante](./assets/blocking.png)

    ```
    let fs = require('fs');

    function blockingExample() {
      console.log("COMIENZO a leer el archivo", Date.now());
      const fileData = fs.readFileSync('rpmFile', 'ascii');
      console.log("TERMINE de leer el archivo", Date.now());

      console.log("HOLA AQUI ESTA EL ARCHIVO", fileData);
    }

    blockingExample();
    console.log("ME EJECUTO AL FINAL");

    ```



 - No Bloqueantes
 
    - De acción no bloqueante del hilo de proceso (por delegación), también conocidas por ser asíncronas.
    - Permite ejecución de otras instrucciones mientras finaliza.

    ```
    let fs = require('fs');

    function nonBlockingExample() {
      console.log("COMIENZO a leer el archivo", Date.now());
      fs.readFile('rpmFile', 'ascii', callback);

      function callback(err, data) {
        if(err) throw err;
        console.log("TERMINE de leer el archivo", Date.now());
        console.log("HOLA AQUI ESTA EL ARCHIVO", data);

        console.log("sigo con alguna instruccion que dependa de la lectura del archivo");
      }
    }

    nonBlockingExample();
    console.log("ME EJECUTO CUANDO MI INTERPRETAN, BECAUSE ASYNC");
    ```
    ---
    La salida de este extracto de código bloqueante es la siguiente:
    ```
    Salida Consola Ejemplo bloqueante:

    COMIENZO a leer el archivo 1559577544080
    TERMINE de leer el archivo 1559577544081
    HOLA AQUI ESTA EL ARCHIVO ASDFGHQWERTYUI123456789

    ME EJECUTO AL FINAL

    ```
    ---
    La salida de este extracto de código no bloqueante es la siguiente:
    ```
    COMIENZO a leer el archivo 1559577544081
    ME EJECUTO CUANDO MI INTERPRETAN, BECAUSE ASYNC
    TERMINE de leer el archivo 1559577544086
    HOLA AQUI ESTA EL ARCHIVO ASDFGHQWERTYUI123456789

    .....
    sigo con alguna instruccion que dependa de la lectura del archivo
      

    ```

-------------------------------- 
## 4.- Eventloop y threadpool

Para que el browser ejecute código, necesita de un motor que provea tal ambiente de ejecución. Por ejemplo, Chrome
viene con el motor V8 de Javascript, desarrollado por ellos.Además del motor, los browsers se valen de las siguientes 
piezas:

1- __Call stack__: Javascript es un lenguaje con un sólo hilo de ejecución, lo cual significa que 
sólo puede manejar una tarea en un determinado mometo.Esta pila de llamadas es una estructura que guarda
las llamadas a funciones. Cuando se ejecuta una función, se coloca la misma en el tope del stack y cuando 
vamos a retornar, lo hacemos desde el tope de la pila. Su mecanismo de procesamiento es FILO(First In Last Out).
Cada entrada en el call stack es un stack frame, cuando se superan los 16,000 frames por stack, se genera un error de Max Stack Error Reached y las ejecuciones quedarán guindadas.
![Call Stack](./assets/stack.gif)

2.- __Heap__: Es una parte de la memoria que se usa para almacenar data arbitraria en una forma desordenada. 
Es dónde se guardan y disponibilizan objetos.

3- __Callback Queue(message Queue, task queue)__: Todas las tareas que no pueden ser ejecutadas en el momento
son guardadas aquí. Generalmente, se almacenan aquí todos los callback asociados a alguna Web Api.

![Callback Queue](./assets/4_callback_queue.png)

4.- __Thread Pool__: Es el conjunto de instrucciones I/O que permiten realizar acciones como
enviar peticiones HTTP, escuchar eventos del DOM, retrazar la ejecución de alguna instrucción con
setTimeout o setInterval, caching o almacenamiento de BD, este tipo de tareas usualmente son las más pesadas en términos de procesamiento. Para realizar esto, el browser usa lenguaje de bajo nivel como C++ que se acercan a lenguaje de máquina y Además
proporciona web Apis con los que interactuar. Estas webApis son asíncronas, y se le deben proveer funciones 
callback de forma que ejecuten algún código Javascript, una vez se ejecute la web Api. 

Cada vez que una WebApi termina su trabajo, envía su callback asociado a la fila de callbacks. Estos permanecen encolados,
hasta que el event loop, los regresa al stack para que se ejecute.

![Thread Pool](./assets/4_thread_pool.png)

5.- __Event Loop__: Es el elemento responsable de llevar a cabo la ejecución de las tareas que queden en el Callback Queue.
Cada vez que encuentra el stack de ejecución vacío, envían una tarea del callback queue a ejecutarse. De esta forma, 
cada tarea es procesada completamente antes de procesar la siguiente tarea.
![Event Loop](./assets/4_event_loop.png)

Aquí podemos ver integrados todos los elementos:
![Javascript Runtime](./assets/4_partes_event_loop.png)

A continuación, valiendonos de esta [herramienta](http://latentflip.com/loupe/?code=ZnVuY3Rpb24gcHJpbnRIZWxsbygpIHsNCiAgICBjb25zb2xlLmxvZygnSGVsbG8gZnJvbSBiYXonKTsNCn0NCg0KZnVuY3Rpb24gYmF6KCkgew0KICAgIHNldFRpbWVvdXQocHJpbnRIZWxsbywgMzAwMCk7DQp9DQoNCmZ1bmN0aW9uIGJhcigpIHsNCiAgICBiYXooKTsNCn0NCg0KZnVuY3Rpb24gZm9vKCkgew0KICAgIGJhcigpOw0KfQ0KDQpmb28oKTs%3D!!!PGJ1dHRvbj5DbGljayBtZSE8L2J1dHRvbj4%3D), ejecutaremos un extracto de código e iremos visualizando cómo se ejecutan las instrucciones:
```
function printHello() {
    console.log('Hello from baz');
}

function baz() {
    setTimeout(printHello, 3000);
}

function bar() {
    baz();
}

function foo() {
    bar();
}

foo();
```

Para visualizar eventos de DOM, usemos el siguiente extracto de código:
```
$.on('button', 'click', function onClick() {
    setTimeout(function timer() {
        console.log('You clicked the button!');    
    }, 2000);
});

console.log("Hi!");

setTimeout(function timeout() {
    console.log("Click the button!");
}, 5000);

console.log("Welcome to loupe.");
```

-------------------------------- 
## 5.- Errores y excepciones

En Javascript, existen estos dos tipos generales de errores:

- Estándar Javascript errors:
Errores de sintaxis, de evaluación, contrucción de URIs, etc.


- User-specified errors
Cuando lanzamos un Throw y especificamos un mensaje personalizado.
Cuando ocurren este tipo de errores, usualmente la cónsola del navegador nos imprime el stack trace del error, que es una foto de la pila de ejecución o call stack que se dio en ese momento:
```
function baz(){
   throw new Error('Something went wrong.');
}
function bar() {
   baz(); 
}
function foo() {
   bar(); 
}
foo();
```
![stack trace](./assets/error.png)

- Cuando superamos el umbral de los 16.000 frames en el stack, el navegador se nos quedará colgado y se generará un error de Max Stack Error Reached.
![max stack](./assets/error2.png)

- La forma correcta de controlar los errores es a través de la instrucción try/catch:
```
try {
  Block of code to try
}
catch(err) {
  Block of code to handle errors
}
```
Ejemplo:
```
try {
  adddlert("Welcome guest!");
}
catch(err) {
  document.getElementById("demo").innerHTML = err.message;
}
```
Como buena práctica, se recomienda evitar lanzar throw dentro de funciones asincronas, porque el catch nunca podrá atraparlos.
La regla sonar que nos permitirá no envolver funciones asincronas en un try/catch es no-try-promise.

Ejemplo:
```
function runPromise() {
  return Promise.reject("rejection reason");
}

function foo() {
  try { 
    runPromise(); 
  } catch (e) {
    console.log("Failed to run promise", e);
  }
}

foo();// Nunca llega al catch: Uncaught (in promise) rejection reason
```

Por el contrario, si la envolvemos con async/await:
```
async function foo() {
  try {
    await runPromise();
  } catch (e) {
    console.log("Failed to run promise", e);
  }
}
```



-------

